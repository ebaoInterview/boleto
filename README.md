PROBLEMA
========
Dado os seguintes arquivos de boletos - representando tabelas no sistema

```text
BOLETO_ID   CREATED_TIME    AMOUNT      DUE_DATE
1           18/01/2019      100,00      20/03/2019
2           18/02/2019      80,00       20/03/2019
3           18/03/2019      10,00       20/03/2019
4           10/01/2019      20,00       15/01/2019
5           12/02/2019      50,00       13/02/2019
6           20/03/2019      50,00       25/03/2019    
```

```text
AJUSTE_ID   BOLETO_ID       STATUS      CREATED_TIME
1           1               ATIVO       20/03/2019
2           2               ATIVO       20/03/2019
3           3               ATIVO       20/03/2019
4           4               ATIVO       15/01/2019
5           5               ATIVO       13/02/2019
6           6               ATIVO       25/03/2019    
7           5               CANCELADO   14/02/2019
8           6               CANCELADO   27/03/2019
```

Resultado esperado
------------------
* Trazer todos os boletos ATIVOS do sistema
* Trazer todos os boletos CANCELADOS do sistema de uma data específica
* Trazer todos os ajustes de um boleto

Observações
------------
* Aplicação deverá ser feita em Java (qualquer versão à sua escolha)
* Seja criativo;
* Uma outra pessoa estará lendo o que você fez, portanto, lembre-se de buscar trazer a semântica do problema no seu código;
* Crie testes;
* Não é necessário utilizar nenhum framework, procure utilizar os recursos da linguagem;

ENTREGA
=======
* **Não** faça um fork desse projeto. Crie um repositório no seu perfil do GitHub e nos envie a url;
* Crie um arquivo SUAS-INSTRUÇÕES.txt para adicionar algum comentário/observação que achar importante;